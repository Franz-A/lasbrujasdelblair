package pe.edu.uni.fiis.lasBrujasDelBlair.service.producto;

import pe.edu.uni.fiis.lasBrujasDelBlair.dao.SingletonDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Producto;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.Conexion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ProductoServiceImpl implements ProductoService {
    public boolean agregarProducto(Producto a){
        Connection connection= Conexion.getConnection();
        boolean usur = SingletonDao.getProductoDao().agregarProducto(a,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usur;

    }
    public boolean agregarProductoCategoria(Producto a){
        Connection connection= Conexion.getConnection();
        boolean usur = SingletonDao.getProductoDao().agregarProductoCategoria(a,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usur;

    }

    public List<Producto> obtenerProductos(){
        Connection connection= Conexion.getConnection();
        List<Producto> list = SingletonDao.getProductoDao().obtenerProductos(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
