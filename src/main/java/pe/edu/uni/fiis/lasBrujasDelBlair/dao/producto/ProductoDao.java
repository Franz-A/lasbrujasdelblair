package pe.edu.uni.fiis.lasBrujasDelBlair.dao.producto;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Producto;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;

import java.sql.Connection;
import java.util.List;

public interface ProductoDao {
    public boolean agregarProducto(Producto a, Connection b);
    public boolean agregarProductoCategoria(Producto a, Connection b);
    public List<Producto> obtenerProductos(Connection b);
}
