package pe.edu.uni.fiis.lasBrujasDelBlair.dao.categoria;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDaoImpl implements CategoriaDao {

    public List<Categoria> obtenerCategorias( Connection b) {
        List<Categoria> lista = new ArrayList<Categoria>();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select id_categoria, nombre, precio from categoria");
            Statement sentencia = b.createStatement();
            ResultSet rs = sentencia.executeQuery(sql.toString());

            while (rs.next()){
                Categoria u = new Categoria();
                u.setIdCategoria(rs.getString("id_categoria"));
                u.setNombre(rs.getString("nombre"));
                u.setPrecio(rs.getDouble("precio"));
                lista.add(u);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Categoria actualizarCategoria(Categoria a, Connection b) {
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("update categoria set precio=?  where id_categoria=?");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setDouble(1, a.getPrecio());
            sentencia.setString(2, a.getIdCategoria());
            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }

}
