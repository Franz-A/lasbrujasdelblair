package pe.edu.uni.fiis.lasBrujasDelBlair.controller;

import pe.edu.uni.fiis.lasBrujasDelBlair.DTO.ProductoDTO;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Producto;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.SingletonService;
import pe.edu.uni.fiis.lasBrujasDelBlair.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ProductoController",urlPatterns = {"/agregar-producto"})
public class ProductoController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);

        Producto producto = Json.getInstance().readValue(data, Producto.class);
        boolean c= SingletonService.getProductoService().agregarProducto(producto);
        boolean d= SingletonService.getProductoService().agregarProductoCategoria(producto);

        Json.envioJson(c&&d,resp);
    }

}