package pe.edu.uni.fiis.lasBrujasDelBlair.dao.usuariof;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioFDaoImpl implements UsuarioFDao {
    public boolean agregarUsuarioF(UsuarioF a, Connection b) {
        boolean c=false;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into usuario( id_usuario, tipo, password,nombres) values (")
                    .append("?,?,?,?);");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,a.getIdUsuario());
            sentencia.setString(2,a.getTipo());
            sentencia.setString(3,a.getPassword());
            sentencia.setString(4,a.getNombres());
            int d= sentencia.executeUpdate();
            c=d>0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }
    public UsuarioF obtenerUsuarioF(String correo, String password, Connection b){
        UsuarioF usuarioF = new UsuarioF();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select  id_usuario, tipo, password,nombres from usuario where id_usuario=? and password=?");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,correo);
            sentencia.setString(2,password);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                usuarioF.setIdUsuario(rs.getString("id_usuario"));
                usuarioF.setTipo(rs.getString("tipo"));
                usuarioF.setPassword(rs.getString("password"));
                usuarioF.setNombres(rs.getString("nombres"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usuarioF;
    }

}