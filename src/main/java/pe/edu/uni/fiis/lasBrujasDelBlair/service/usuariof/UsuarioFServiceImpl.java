package pe.edu.uni.fiis.lasBrujasDelBlair.service.usuariof;


import pe.edu.uni.fiis.lasBrujasDelBlair.dao.SingletonDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.Conexion;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.usuariof.UsuarioFService;

import java.sql.Connection;
import java.sql.SQLException;

public class UsuarioFServiceImpl implements UsuarioFService {
    public boolean agregarUsuarioF(UsuarioF usuarioF) {
        Connection connection= Conexion.getConnection();
        boolean usur = SingletonDao.getUsuarioFDao().agregarUsuarioF(usuarioF,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usur;
    }
    public UsuarioF obtenerUsuarioF(String correo, String password) {
        Connection connection= Conexion.getConnection();
        UsuarioF usur = SingletonDao.getUsuarioFDao().obtenerUsuarioF(correo, password, connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usur;
    }

}
