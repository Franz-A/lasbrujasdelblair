package pe.edu.uni.fiis.lasBrujasDelBlair.service.usuariof;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;

public interface UsuarioFService {
    public boolean agregarUsuarioF(UsuarioF usuariof);
    public UsuarioF obtenerUsuarioF(String correo, String password);
}
