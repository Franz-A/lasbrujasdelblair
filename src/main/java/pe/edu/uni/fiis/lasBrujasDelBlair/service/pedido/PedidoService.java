package pe.edu.uni.fiis.lasBrujasDelBlair.service.pedido;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Pedido;

import java.util.List;

public interface PedidoService {
    public List<Pedido> agregarPedido(Pedido pedido);
    public List<Pedido> obtenerPedidos();
}
