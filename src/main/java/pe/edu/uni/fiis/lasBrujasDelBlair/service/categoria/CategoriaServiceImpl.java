package pe.edu.uni.fiis.lasBrujasDelBlair.service.categoria;

import pe.edu.uni.fiis.lasBrujasDelBlair.dao.SingletonDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.Conexion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CategoriaServiceImpl implements  CategoriaService {
    @Override
    public List<Categoria> obtenerCategorias() {
        Connection connection= Conexion.getConnection();
        List<Categoria> list = SingletonDao.getCategoriaDao().obtenerCategorias(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Categoria actualizarCategorias(Categoria categoria) {
        Connection connection= Conexion.getConnection();
        SingletonDao.getCategoriaDao().actualizarCategoria(categoria,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categoria;
    }
}
