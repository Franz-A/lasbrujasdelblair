package pe.edu.uni.fiis.lasBrujasDelBlair.service;


import pe.edu.uni.fiis.lasBrujasDelBlair.service.categoria.CategoriaService;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.categoria.CategoriaServiceImpl;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.pedido.PedidoService;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.pedido.PedidoServiceImpl;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.producto.ProductoService;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.producto.ProductoServiceImpl;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.usuariof.UsuarioFService;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.usuariof.UsuarioFServiceImpl;

public class SingletonService {

    private static UsuarioFService usuarioFService = null;

    public static UsuarioFService getUsuarioFService() {
        if (usuarioFService == null) {
            usuarioFService = new UsuarioFServiceImpl();
        }
        return usuarioFService;
    }


    private static CategoriaService categoriaService = null;

    public static CategoriaService getCategoriaService() {
        if (categoriaService == null) {
            categoriaService = new CategoriaServiceImpl();
        }
        return categoriaService;
    }

    private static ProductoService productoService = null;

    public static ProductoService getProductoService() {
        if (productoService == null) {
            productoService = new ProductoServiceImpl();
        }
        return productoService;
    }

    private static PedidoService pedidoService = null;

    public static PedidoService getPedidoService() {
        if (pedidoService == null) {
            pedidoService = new PedidoServiceImpl();
        }
        return pedidoService;
    }
}
