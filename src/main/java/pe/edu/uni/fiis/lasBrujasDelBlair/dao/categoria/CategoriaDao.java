package pe.edu.uni.fiis.lasBrujasDelBlair.dao.categoria;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;

import java.sql.Connection;
import java.util.List;

public interface CategoriaDao {
    public abstract List<Categoria> obtenerCategorias( Connection b);
    public Categoria actualizarCategoria(Categoria a, Connection b);
}
