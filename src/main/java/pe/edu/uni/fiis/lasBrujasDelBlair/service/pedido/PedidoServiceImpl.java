package pe.edu.uni.fiis.lasBrujasDelBlair.service.pedido;


import pe.edu.uni.fiis.lasBrujasDelBlair.dao.SingletonDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Pedido;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.Conexion;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class PedidoServiceImpl implements PedidoService {
    public List<Pedido> agregarPedido(Pedido pedido) {
        Connection connection= Conexion.getConnection();
        SingletonDao.getPedidoDao().agregarPedido(pedido,connection);
        List<Pedido> list=SingletonDao.getPedidoDao().obtenerPedidos(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Pedido> obtenerPedidos() {
        Connection connection= Conexion.getConnection();
        List<Pedido> list = SingletonDao.getPedidoDao().obtenerPedidos(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
