package pe.edu.uni.fiis.lasBrujasDelBlair.controller;

import pe.edu.uni.fiis.lasBrujasDelBlair.DTO.PedidoDTO;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Pedido;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.SingletonService;
import pe.edu.uni.fiis.lasBrujasDelBlair.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PedidoController",urlPatterns = {"/agregar-pedido"})
public class PedidoController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);

        Pedido pedido = Json.getInstance().readValue(data,Pedido.class);

        List<Pedido> pedidos = SingletonService.getPedidoService().agregarPedido(pedido);

        PedidoDTO pedidoDTO = new PedidoDTO();
        pedidoDTO.setPedidos(pedidos);
        Json.envioJson(pedidoDTO,resp);
    }

}
