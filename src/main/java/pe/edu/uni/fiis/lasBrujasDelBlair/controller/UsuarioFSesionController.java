package pe.edu.uni.fiis.lasBrujasDelBlair.controller;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Login;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.SingletonService;
import pe.edu.uni.fiis.lasBrujasDelBlair.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UsuarioFSesionController",urlPatterns = {"/iniciar-sesion"})
public class UsuarioFSesionController extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);

        Login login = Json.getInstance().readValue(data, Login.class);
        UsuarioF usuarioF = SingletonService.getUsuarioFService().obtenerUsuarioF(login.getCorreo(),login.getPassword());

        Json.envioJson(usuarioF,resp);
    }
}
