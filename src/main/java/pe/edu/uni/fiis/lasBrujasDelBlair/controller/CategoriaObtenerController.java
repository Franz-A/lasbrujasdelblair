package pe.edu.uni.fiis.lasBrujasDelBlair.controller;

import pe.edu.uni.fiis.lasBrujasDelBlair.DTO.CategoriaDTO;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;
import pe.edu.uni.fiis.lasBrujasDelBlair.service.SingletonService;
import pe.edu.uni.fiis.lasBrujasDelBlair.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CategoriaObtenerController",urlPatterns = {"/obtener-categorias"})
public class CategoriaObtenerController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);

        UsuarioF usuarioF = Json.getInstance().readValue(data,UsuarioF.class);

        List<Categoria> lista = SingletonService.getCategoriaService().obtenerCategorias();
        CategoriaDTO categoriaDTO = new CategoriaDTO();
        categoriaDTO.setCategorias(lista);
        Json.envioJson(categoriaDTO,resp);
    }
}