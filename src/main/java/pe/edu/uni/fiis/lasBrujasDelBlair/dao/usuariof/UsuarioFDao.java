package pe.edu.uni.fiis.lasBrujasDelBlair.dao.usuariof;



import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;

import java.sql.Connection;

public interface UsuarioFDao {
    public boolean agregarUsuarioF(UsuarioF a, Connection b);

    public UsuarioF obtenerUsuarioF(String correo, String password, Connection b);
}
