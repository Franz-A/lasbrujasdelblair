package pe.edu.uni.fiis.lasBrujasDelBlair.dao.pedido;



import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Pedido;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Producto;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.UsuarioF;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PedidoDaoImpl implements PedidoDao {
    public Pedido agregarPedido(Pedido a, Connection b) {
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into pedido(id_usuario,id_pedido,cantidad)" +
                    " values (?,?,?);")
.append("insert into pedido_producto(id_pedido,id_producto,id_producto_categoria)"+
                           " values (?,?,?),(?,?,?),(?,?,?); ");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,a.getUsuarioF().getIdUsuario());
            sentencia.setString(2,a.getIdPedido());
            sentencia.setInt(3,a.getCantidad());

            sentencia.setString(4,a.getIdPedido());
            sentencia.setString(5,a.getProductos().get(0).getIdProducto());
            sentencia.setString(6,a.getProductos().get(0).getIdProducto()+a.getCategoria().getNombre());

            sentencia.setString(7,a.getIdPedido());
            sentencia.setString(8,a.getProductos().get(1).getIdProducto());
            sentencia.setString(9,a.getProductos().get(1).getIdProducto()+a.getCategoria().getNombre());

            sentencia.setString(10,a.getIdPedido());
            sentencia.setString(11,a.getProductos().get(2).getIdProducto());
            sentencia.setString(12,a.getProductos().get(2).getIdProducto()+a.getCategoria().getNombre());
            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }

    @Override
    public List<Pedido> obtenerPedidos(Connection b) {
        return null;
    }


/*
  public List<Pedido> obtenerPedidos (Connection b) {
        List<Pedido> lista = new ArrayList<Pedido>();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select pe.id_usuario, pe.id_pedido, " +
                    " pe.cantidad,pr.nombre,pr.tipo " +
                    " pr.id_categoria, c.id_categoria" +
                    " from pedido pe" +
                    " inner join producto pr " +
                    " on (pe.id_producto=pr.id_producto)" +
                    " inner join categoria c" +
                    " on (c.id_categoria=pr.id_categoria);");

            Statement sentencia = b.createStatement();
            ResultSet rs = sentencia.executeQuery(sql.toString());
            while (rs.next()){
                Pedido u = new Pedido();
                u.setProducto(new Producto());
                u.setUsuarioF(new UsuarioF());
                u.getUsuarioF().setIdUsuario(rs.getString("id_usuario"));
                u.setIdPedido(rs.getString("id_pedido"));
                u.getProducto().setNombre(rs.getString("nombre"));
                u.getProducto().setCategoria(new Categoria());
                u.getProducto().getCategoria().setIdCategoria("id_categoria");
                u.getProducto().getCategoria().setNombre("id_categoria");
                lista.add(u);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

*/
}