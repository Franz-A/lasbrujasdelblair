create database sustitutorio
    with owner postgres;

create table usuario
(
    id_usuario varchar(60) not null
        constraint usuario_pk
            primary key,
    tipo       varchar(60),
    password   varchar(128),
    nombres    varchar(60)
);

alter table usuario
    owner to postgres;

create table categoria
(
    id_categoria varchar(60) not null
        constraint categoria_pk
            primary key,
    nombre       varchar(60),
    precio       numeric(5, 2)
);

alter table categoria
    owner to postgres;

create table producto
(
    id_producto varchar(60) not null
        constraint producto_pk
            primary key,
    nombre      varchar(60),
    tipo        varchar(60)
);

alter table producto
    owner to postgres;

create table producto_categoria
(
    id_producto           varchar(60)
        constraint "producto-categoria_producto_id_producto_fk"
            references producto,
    id_categoria          varchar(60)
        constraint "producto-categoria_categoria_id_categoria_fk"
            references categoria,
    id_producto_categoria varchar(60) not null
        constraint "producto-categoria_pk"
            primary key,
    cantidad              numeric(3)
);

alter table producto_categoria
    owner to postgres;

create table pedido
(
    id_usuario            varchar(60)
        constraint pedido_usuario_codigo_fk
            references usuario,
    id_pedido             varchar(60) not null
        constraint pedido_pk
            primary key,
    id_producto_categoria varchar(60)
        constraint "pedido_producto-categoria_id_producto_categoria_fk"
            references producto_categoria,
    cantidad              numeric(3)
);

alter table pedido
    owner to postgres;


INSERT INTO categoria (id_categoria, nombre, precio) VALUES ('Dieta', 'Dieta', null);
INSERT INTO categoria (id_categoria, nombre, precio) VALUES ('Regular', 'Regular', null);
INSERT INTO categoria (id_categoria, nombre, precio) VALUES ('Extra', 'Extra', null);
INSERT INTO usuario (id_usuario, tipo, password,nombres) VALUES ('Master', 'Master', 'Master','Master');
