var app = new Vue({
    el: '#app',
    data: {
        mensaje: 'Hello Vue!',
        isActive: false,
        isMaster: false,
        isUsuario:false,
        titulo: 'Registro de Usuario',
        pagina: 'registro',//pagina inicial
        usuario: {// para el registro
            nombres: null,
            idUsuario: null,
            tipo: null,
            password: null,
        },

        login: {
            correo: null,
            password: null,
        },
        usuarioF: {// para
            nombres: null,
            idUsuario: null,
            tipo: null,
            password: null,
        },
        lista:null,
        revisar: null,
        editar: false,

        anadirProducto: false,
        anadirPedido: false,

        producto:{
            idProducto:null,
            nombre:null,
            tipo:null,
            cantidad:null,
            categoria:{
                idCategoria:null,
                nombre:null,
                precio:null,
            },
        },

        pedido:{
            idPedido:null,
            usuarioF:{
                nombres: null,
                idUsuario: null,
                tipo: null,
                password: null,
            },
            productos:[
                {
                    idProducto:null,
                    nombre:null,
                    tipo:null,
                    cantidad:null,
                    categoria:{
                        idCategoria:null,
                        nombre:null,
                        precio:null,
                    },
                },
                {
                    idProducto:null,
                    nombre:null,
                    tipo:null,
                    cantidad:null,
                    categoria:{
                        idCategoria:null,
                        nombre:null,
                        precio:null,
                    },
                },
                {
                    idProducto:null,
                    nombre:null,
                    tipo:null,
                    cantidad:null,
                    categoria:{
                        idCategoria:null,
                        nombre:null,
                        precio:null,
                    },
                }
            ],

            categoria:null,
            cantidad:null,
        },

        listaCategorias:[
            {nombre:'Dieta'},
            {nombre:'Regular'},
            {nombre:'Extra'},
        ],
        listaTiposProductos:[
            {tipo:'Ensalada', categoria:'Dieta'},
            {tipo:'Plato principal', categoria:'Dieta'},
            {tipo:'Bebida mineral', categoria:'Dieta'},

            {tipo:'Ensalada', categoria:'Regular'},
            {tipo:'Consome', categoria:'Regular'},
            {tipo:'Plato principal', categoria:'Regular'},
            {tipo:'Postre', categoria:'Regular'},

            {tipo:'Ensalada', categoria:'Extra'},
            {tipo:'Consome', categoria:'Extra'},
            {tipo:'Aperitivo', categoria:'Extra'},
            {tipo:'Plato principal', categoria:'Extra'},
            {tipo:'Postre', categoria:'Extra'},
            {tipo:'Bebida mineral', categoria:'Extra'},
        ],
        listaProductos: null,
    },
    methods: {
        setAnadirPedido:function (){
            this.anadirPedido=true;
            this.pedido={
                idPedido:null,
                usuarioF:{
                    nombres: null,
                    idUsuario: null,
                    tipo: null,
                    password: null,
                },
                productos:[
                    {
                        idProducto:null,
                        nombre:null,
                        tipo:null,
                        cantidad:null,
                        categoria:{
                            idCategoria:null,
                            nombre:null,
                            precio:null,
                        },
                    },
                    {
                        idProducto:null,
                        nombre:null,
                        tipo:null,
                        cantidad:null,
                        categoria:{
                            idCategoria:null,
                            nombre:null,
                            precio:null,
                        },
                    },
                    {
                        idProducto:null,
                        nombre:null,
                        tipo:null,
                        cantidad:null,
                        categoria:{
                            idCategoria:null,
                            nombre:null,
                            precio:null,
                        },
                    }
                ],

                categoria:null,
                cantidad:null,
            },
            this.obtenerProductos();
            this.obtenerCategoria();
        },
        setAnadirProducto:function (){
            this.anadirProducto=true;
        },
        cerrar:function(){
            this.anadirProducto=false;
        },
        cerrarPedido:function(){
            this.anadirPedido=false;
        },
        mostrarProductos: function(){
            this.setPagina('productos');
            this.obtenerCategoria();
        },
        setPagina: function (pagina) {
            this.pagina = pagina;
        },
        isPagina: function (pagina) {
            return (this.pagina == pagina);
        },
        abrirRegistro: function(){
            this.setPagina('registro');
            this.isActive=true;
        },
        abrirIniciarSesion: function(){
            this.setPagina('iniciarSesion');
            this.isActive=true;
        },
        close: function() {
            this.isActive = false;
        },
        // Registra un usuario nuevo
        registrarUsuario: function () {
            let self = this;

            this.usuario.tipo='Cliente';
            if(this.isMaster)
                this.usuario.tipo='Mesero';


            fetch('registrar-usuario', {
                method: 'POST',
                body: JSON.stringify(this.usuario),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
                console.log(data);
                if (data) {
                    self.mensaje='Se ha registrado correctamente';
                    if(self.isMaster)
                        self.isActive = false;
                    else
                        self.pagina = 'iniciarSesion';
                } else {
                    self.mensaje="Ha ocurrido un error. \n Puede que el usuario o correo ya ha sido registrado";
                }
            });
        },

        iniciarSesion: function () {
            let self = this;
            fetch('iniciar-sesion', {
                method: 'POST',
                body: JSON.stringify(this.login),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(function (data) {
                    self.usuarioF = data;
                    if (self.usuarioF.idUsuario!= null) {
                        self.mensaje="Bienvenido:  ";
                        self.mensaje=self.mensaje.concat(self.usuarioF.codigo);
                        self.isActive = false;
                        if(self.usuarioF.tipo=='Master'){
                            self.isMaster=true;
                        }
                        else{
                            if(self.usuarioF.tipo=='Cliente'||self.usuarioF.tipo=='Mesero'){
                                self.isUsuario=true;
                            }
                        }
                        window.localStorage.setItem("user", JSON.stringify(data));
                    } else {
                        self.mensaje="Usuario o Contraseña Incorrecto";
                        self.isActive = true;
                    }
                });
        },
        // Si la sesión está iniciada, nos lleva a la pagina de menu
        revisarSesion: function () {
            let data = window.localStorage.getItem("user");
            if (data) {
                this.isUsuario=true;
                this.usuarioF = JSON.parse(data);
                if(this.usuarioF.tipo=='Master'){
                    this.isMaster=true;
                }
                else{
                    if(this.usuarioF.tipo=='Cliente'||this.usuarioF.tipo=='Mesero'){
                        this.isUsuario=true;
                    }
                }
            }
        },
        // Cierra la sesión
        cerrarSesion: function () {
            window.localStorage.removeItem("user");
            this.isUsuario=false;
            this.isMaster=false;
            this.usuarioF.nombres = null;
            this.usuarioF.codigo = null;
            this.usuarioF.correo = null;
            this.usuarioF.password = null;
        },

        obtenerCategoria: function () {
            let self = this;
            fetch('obtener-categorias', {
                method: 'POST',
                body: JSON.stringify(this.usuarioF),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(function (data) {
                    self.lista = data.categorias;
                });
        },

        obtenerCategorias: function () {
            let self = this;
            fetch('obtener-categorias', {
                method: 'POST',
                body: JSON.stringify(this.usuarioF),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(function (data) {
                    self.lista = data.categorias;
                });
            this.revisarCategorias();
        },

        actualizarCategorias: function (categoria) {
            let self = this;
            fetch('actualizar-categoria', {
                method: 'POST',
                body: JSON.stringify(categoria),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(function (data) {
                    self.lista = data.categorias;
                });

        },

        revisarCategorias: function () {
            if(this.lista[0].precio==0||this.lista[1].precio==0||this.lista[2].precio==0)
            {
                this.revisar= true;
            }
            else
                this.revisar=false;
        },
        editarPrecio: function () {
            this.editar=true;
            this.obtenerCategorias();
        },

        registrarProducto: function () {
            let self = this;
            fetch('agregar-producto', {
                method: 'POST',
                body: JSON.stringify(this.producto),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
                console.log(data);
                if (data) {
                    self.anadirPedido=true;
                    self.mensaje='Se ha añadido el producto correctamente';
                } else {
                    self.mensaje="Ha ocurrido un error.";
                }
            });
        },
        registrarPedido: function () {
            this.pedido.usuarioF.idUsuario=this.usuarioF.idUsuario;
            let self = this;
            fetch('agregar-pedido', {
                method: 'POST',
                body: JSON.stringify(this.pedido),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
                console.log(data);
                self.anadirPedido=false;
            });
        },

        obtenerProductos: function () {
            let self = this;
            fetch('obtener-productos', {
                method: 'POST',
                body: JSON.stringify(this.listaProductos),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
                console.log(data.productos);
                self.listaProductos=data.productos;
            });
        },
    },


    created: function () {
        this.revisarSesion();
    },
});
Vue.config.devtools = true;
document.addEventListener('DOMContentLoaded', () => {
    (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
        let $notification = $delete.parentNode;
        $delete.addEventListener('click', () => {
            $notification.parentNode.removeChild($notification);
        });
    });
});